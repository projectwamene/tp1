/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modèle;

/**
 *
 * @author Jérémy
 */


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;



public class Labyrinthe {
    
       private int tailleX, tailleY ; //dimensions du labyrinthe
       private int departX, departY ; //coord de la position initiale
       private int arriveeX, arriveeY ; //coord de la position finale
       private int posX, posY ; //coord de la position actuelle
       
       
       
       Labyrinthe(){
       };
       
       public int getCurrentPositionX() // Donne la position courante en posX 
               {
                    return posX;
               }; 
      
       public int getCurrentPositionY() // Donne la position courante en posY 
               {
                    return posY;
               }; 
       
       
       public void setCurrentPositionX(int newPosX) // Donne la position courante en posX 
               {
                    posX=newPosX;
               }; 
      
       public void setCurrentPositionY(int newPosY) // Donne la position courante en posY 
               {
                    posY=newPosY;
               };
       public void setTailleX(int newtx) // Donne la position courante en posY 
               {
                    tailleX=newtx;
               };
       public void setTailleY(int newty) // Donne la position courante en posY 
               {
                    tailleY=newty;
               };
       public void setDepartX(int newdx) // Donne la position courante en posY 
               {
                    departX=newdx;
               };
       public void setDepartY(int newdy) // Donne la position courante en posY 
               {
                    departY=newdy;
               };
       public void setArriveeX(int newax) // Donne la position courante en posY 
               {
                    arriveeX=newax;
               };
       public void setArriveeY(int neway) // Donne la position courante en posY 
               {
                    arriveeY=neway;
               };
       
       /** Lit un labyrinthe avec un fichier en paramètre (voir classe File en annexe) au format décrit plus  haut
        Initialise tous les attributs avec les valeurs lues dans le fichier puis instancie la collection de cases et chaque case
        Déclenche l’exception FileFormatException si le fichier ne peut être lu ou si son format est incorrect */ 
       public void initFromFile(File lab) throws FileFormatException
       {
           //checker si le fichier existe
           try
           {    
               
         Scanner sc = new Scanner(lab+".txt");
         
         String tx,ty;
         String dx,dy;
         String ax,ay;
         
         //on récupère la taille du labyrinthe ainsi que les coord de départ et d'arrivée
         tx=sc.next();
         ty=sc.next();
         dx=sc.next();
         dy=sc.next();
         ax=sc.next();
         ay=sc.next();
         
         //conversion de la chaine de caractère récupérée en int
         int taiX = Integer.parseInt(tx);
         int taiY = Integer.parseInt(ty);
         int depX = Integer.parseInt(dx);
         int depY = Integer.parseInt(dy);
         int arrX = Integer.parseInt(ax);
         int arrY = Integer.parseInt(ay);
         
         //on modifie la valeur des paramètres via les setters
         setTailleX(taiX);
         setTailleY(taiY);
         setDepartX(depX);
         setDepartY(depY);
         setArriveeX(arrX);
         setArriveeY(arrY);
           
           }
           catch (FileFormatException e)
           {
             e.printStackTrace(); 
             System.out.println(e.getMessage()); 
           }
           
       };    
 
    /** Tente de bouger le curseur dans la case (x, y) en paramètres.
     Déclenche l’exception ImpossibleMoveException si la case déborde du labyrinthe ou si on ne peut pas aller
     dans la case : voir la méthode canMoveToCase() de la classe Case */ 
       public void move (int x, int y) throws ImpossibleMoveException
       {
           
           
           
       };  
 
    /**  Se déplace aléatoirement d’une seule case (direction en x et y aléatoire) de la position courante (posX, posY)
        sauf si ce déplacement sort du labyrinthe ou va dans un mur */ 
       public void autoMove() 
       {
       
       };

