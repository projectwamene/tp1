/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modèle;

/**
 *
 * @author Jérémy
 */
public class CaseImplementee implements Case{
    protected int posX,posY; //coord de la case
    protected boolean vasy; //1 si c'est une case libre, 0 si c'est un mur
    
   public CaseImplementee(){
       
       
    }
    
    @Override
   public boolean canMoveToCase() // implementation de la methode canmove
   { return vasy; }
   
    @Override
   public int getPositionX()
   { return posX;}
   
    @Override
   public int getPositionY()
   { return posY;}
}
