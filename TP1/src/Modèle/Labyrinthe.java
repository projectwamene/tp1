/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modèle;

/**
 *
 * @author Jérémy
 */


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;



public class Labyrinthe {
    
       private int m_tailleX, m_tailleY ; //dimensions du labyrinthe
       private int m_departX, m_departY ; //coord de la position initiale
       private int m_arriveeX, m_arriveeY ; //coord de la position finale
       private int m_posX, m_posY ; //coord de la position actuelle
       private ArrayList<ArrayList<Case> > m_laby;
       
       
       public Labyrinthe(){
           m_laby=new ArrayList< ArrayList <Case> >();
       };
       public int getCurrentPositionX() // Donne la position courante en posX 
               { return m_posX; }; 
      
       public int getCurrentPositionY() // Donne la position courante en posY 
               { return m_posY; }; 
       
       
       public void setCurrentPositionX(int newPosX) // Donne la position courante en posX 
                { m_posX=newPosX; }; 
      
       public void setCurrentPositionY(int newPosY) // Donne la position courante en posY 
               {
                    m_posY=newPosY;
               };
       public void setTailleX(int newtx) // Donne la position courante en posY 
               {
                    m_tailleX=newtx;
               };
       public void setTailleY(int newty) // Donne la position courante en posY 
               {
                    m_tailleY=newty;
               };
       public void setDepartX(int newdx) // Donne la position courante en posY 
               {
                    m_departX=newdx;
               };
       public void setDepartY(int newdy) // Donne la position courante en posY 
               {
                    m_departY=newdy;
               };
       public void setArriveeX(int newax) // Donne la position courante en posY 
               {
                    m_arriveeX=newax;
               };
       public void setArriveeY(int neway) // Donne la position courante en posY 
               {
                    m_arriveeY=neway;
               };
       
       public int getTailleX()
       {
         return m_tailleX;  
       };
       public int getTailleY()
       {
         return m_tailleY;  
       };
       public int getArriveeX()
       {
         return m_arriveeX;  
       };
       public int getArriveeY()
       {
         return m_arriveeY;  
       }
       public ArrayList<ArrayList<Case>> getLab()
       {
         return m_laby;  
       }
       
       /** Lit un labyrinthe avec un fichier en paramètre (voir classe File en annexe) au format décrit plus  haut
        Initialise tous les attributs avec les valeurs lues dans le fichier puis instancie la collection de cases et chaque case
        Déclenche l’exception FileFormatException si le fichier ne peut être lu ou si son format est incorrect 
     */ 
       public void initFromFile(File lab) throws FileNotFoundException 
               
       {
           if (!lab.canExecute())
           {
               throw new FileNotFoundException();
           }
           else
           {
               Scanner sc = new Scanner(lab);
           String tx,ty;
           String dx,dy;
           String ax,ay;   
           //on récupère les données du fichier
           tx=sc.next();
           ty=sc.next();
           dx=sc.next();
           dy=sc.next();
           ax=sc.next();
           ay=sc.next();
           //conversion en int
           int taiX = Integer.parseInt(tx);
           int taiY = Integer.parseInt(ty);
           int depX = Integer.parseInt(dx);
           int depY = Integer.parseInt(dy);
           int arrX = Integer.parseInt(ax);
           int arrY = Integer.parseInt(ay);
           //on modif les attributs du labyrinthe
           setTailleX(taiX);
           setTailleY(taiY);
           setDepartX(depX);
           setDepartY(depY);
           setArriveeX(arrX);
           setArriveeY(arrY);
           setCurrentPositionX(depX);
           setCurrentPositionY(depY);
           int i=0;
           while (sc.hasNext())
           {
               m_laby.add(new ArrayList<Case>());
               String temp1=sc.nextLine();
               for (int j=0; j<temp1.length(); j++)
               {
                   char temp2=temp1.charAt(j);
                   if (temp2=='X')
                   {
                       m_laby.get(i).add(new CaseMur(i, j));
                   }
                   else
                   {
                       m_laby.get(i).add(new CaseTrou(i, j));
                   }                  
               }
               i++;
           }
           }
           
           
       };    
 
    /** Tente de bouger le curseur dans la case (x, y) en paramètres.
     Déclenche l’exception ImpossibleMoveException si la case déborde du labyrinthe ou si on ne peut pas aller
     dans la case : voir la méthode canMoveToCase() de la classe Case 
     */ 
       public boolean move (int x, int y) 
       {
           if (x<m_tailleX && y<m_tailleY && x>=0 && y>=0)
           {
               if (m_laby.get(x+1).get(y).canMoveToCase() && x<m_tailleX && y<m_tailleY && x>=0 && y>=0)
             {
                 m_posX=x;
                 m_posY=y;
                 return true;
             }
               else 
               {
                   System.out.println("Impossible d'aller sur cette case mamene");
               }
           }
           else 
             {
                 System.out.println("Impossible d'aller sur cette case mamene");
             }
           return false;
       };  
 
    /**  Se déplace aléatoirement d’une seule case (direction en x et y aléatoire) de la position courante (posX, posY)
        sauf si ce déplacement sort du labyrinthe ou va dans un mur */ 
       public void automove() 
       {
        Random rand = new Random(); 
        
        int x=m_posX;
        int y=m_posY;
        boolean test=false;
        while (!test)
        {
            int alea=rand.nextInt(4);
            switch(alea)
        {
            case 0:
                //bouger à gauche
                test=move(x-1,y);
                break;
            case 1:
               
               test=move(x+1,y);
               break;
              
            case 2:
                
                test=move(x,y-1);
                break;
               
            case 3:
                
                test=move(x,y+1);
                break;
             
        }
        }
       };

   
}

       

