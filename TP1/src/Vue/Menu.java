/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vue;

/**
 *
 * @author Jérémy 
 */


import static java.lang.System.exit;
import java.util.Scanner;
import Modèle.Labyrinthe;

public class Menu {
    
    public Menu(){
    };
    
    public void affichageMenu(Labyrinthe lab)
    {
        
        int choix;
        String temp;
        System.out.println("Que souhaitez-vous faire?");
        System.out.println("1. me déplacer en haut ");
        System.out.println("2. me déplacer en bas ");
        System.out.println("3. me déplacer à gauche");
        System.out.println("4. me déplacer à droite");
        System.out.println("5. me déplacer en automove");
        System.out.println("6. quitter");
        
        Scanner sc = new Scanner(System.in);
        temp=sc.next();
        choix=Integer.parseInt(temp);
        int x=lab.getCurrentPositionX();
        int y=lab.getCurrentPositionY();
        switch(choix)
        {
            case 1:
                //bouger à gauche
                
                lab.move(x-1,y);
                break;
            case 2:
               
               lab.move(x+1,y);
               break;
              
            case 3:
                
                lab.move(x,y-1);
                break;
               
            case 4:
                
                lab.move(x,y+1);
                break;
            case 5:
                 lab.automove();
                break;
            case 6:
                 exit(0);
                break;
        }
    };
    public void affichageLabyrinthe(Labyrinthe lab)
    {
        
        for (int i=0; i<lab.getLab().size(); i++)
        {
   
            for (int j=0; j<lab.getLab().get(i).size(); j++)
            {
                
                if (lab.getLab().get(i).get(j).canMoveToCase())
                {
                   
                    if (lab.getCurrentPositionX()==i-1 && lab.getCurrentPositionY()==j)
                    {
                        
                        System.out.print("P");
                    }
                    else 
                    {
                        System.out.print("_");
                    }
                }
                else 
                {
                    System.out.print("X");
                }
            }
            System.out.print("\n");
            
        }
    }
}
