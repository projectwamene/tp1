/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Contrôleur;

/**
 *
 * @author Jérémy
 */

import Modèle.Labyrinthe;
import Vue.Menu;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;


public class TP1 {

    /**
     * @param args the command line arguments
     */
    
    
    public static void main(String[] args) throws FileNotFoundException {
        // TODO code application logic here
        
        //demander un nom de fichier
        File fic;
        do
        {
   
        System.out.println("Veuillez saisir le nom du fichier où se trouve le labyrinthe");
        Scanner sc = new Scanner (System.in);
        String str = sc.next();
        str += ".txt";
        fic = new File(str);
        } while (!fic.canExecute());
           
        
        
        
        //création d'un labyrinthe
        
        Labyrinthe lab = new Labyrinthe();
        Menu m = new Menu();
        
        lab.initFromFile(fic);      
        
        
        //tant que la partie n'est pas gagné, càd tant que le joueur la position actuelle est différente de la position d'arrivée
        while(!(lab.getCurrentPositionX()==lab.getArriveeX() && lab.getCurrentPositionY()==lab.getArriveeY()))
        {
        m.affichageLabyrinthe(lab);
        m.affichageMenu(lab);
        //effacer l'écran
        }
        System.out.println("Tu as gagné mamene, reviens vite pour de nouvelles aventures !");
    }
}

   
